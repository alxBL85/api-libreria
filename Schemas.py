from datetime import date
from pydantic import BaseModel

class Autor(BaseModel):
    nombre: str
    apellido: str
    pais: str
    nacimiento: date
    fallecimiento: date = None


class Editorial(BaseModel):
    nombre: str
    pais: str
