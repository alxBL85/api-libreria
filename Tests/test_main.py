from fastapi.testclient import TestClient
from main import app # instancia de fast api

client = TestClient(app)

def test_isAlive():
    response = client.get("/isalive")
    assert response.status_code == 200
    assert response.json() == ['yes']

# pruebas unitarias
def test_version():
    response = client.get("/version")
    assert response.status_code == 200 # verifica una condicion, si true, continua; si false, emite un error
    resp = response.json()
    print(resp)
    assert resp["curso"] == "Python"

# pruebas end to end
def test_autor():
    # validar que /autores retorne más de 0 valores
     # /autores
     response = client.get("/v1/autores")
     assert response.status_code == 200
     resp = response.json()
     autores = resp["autores"]
     assert autores is not None and len(autores) >= 0

    # validar a crear un autor
     # post /autor
     response2 = client.post(url = "/v1/autor",
                             json = {
                                 "nombre": "John",
                                 "apellido": "Tolkien",
                                 "pais": "Inglaterra",
                                 "nacimiento": "1910-05-10",
                                 "fallecimiento": "1980-06-20"
                             }
                             )

     assert response2.status_code == 201
     resp_autor = response2.json()
     assert resp_autor["created"] == "ok"
     id_autor = resp_autor["id"]



    # validar a obtener el autor creado
     # /autores?nombre="José"
     resp_autores = client.get("/v1/autores")
     autores = resp_autores.json()["autores"]
     filtro_autores = list(filter(lambda aut: aut[0] == id_autor, autores))
     assert len(filtro_autores) == 1 and filtro_autores[0][1] == "John"



    # validar a modificar el autor creado
      # put /autor

    # validar a borrar el autor creado
      # delete /autor
     resp_delete_autor = client.delete(f"/v1/autor/{id_autor}")
     assert resp_delete_autor.json()["deleted"] == "ok"