from fastapi import FastAPI, Depends, status, HTTPException
import psycopg2
from typing import List

from sqlalchemy.orm import Session

from sql_app import crud, models, schemas, database, main
from sql_app.database import SessionLocal, engine


app_v2 = FastAPI(root_path="/v2")

@app_v2.on_event("startup")
async def startup():
    print(">>> Starting the API")

@app_v2.on_event("shutdown")
async def shutdown():
    print("<<< Stopping the API")

# ========================================================================================



# /v2/categorias
@app_v2.get("/raw/categorias")
async def get_categorias():
    con = psycopg2.connect("host=127.0.0.1 dbname=libreria port=5432 user=con_app_libreria password=L1br3r1a")
    cur = con.cursor()

    sql_query = "SELECT id, nombre FROM categoria"
    cur.execute(sql_query)
    resultados = cur.fetchall()
    cur.close()
    con.close()
    return {"categorias": resultados}

# ==============================================================================================================

@app_v2.get("/orm/categorias", response_model = List[schemas.Categoria_Get], status_code=status.HTTP_200_OK)
def read_categorias(skip: int = 0, limite: int = 100, db: Session = Depends(main.get_db)):
    try:
        categorias = crud.get_categorias(db, skip, limite)
        return categorias
    except:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not Found")

@app_v2.get("/orm/categoria/{id}", response_model = schemas.Categoria_Get, status_code= status.HTTP_200_OK)
def read_categoria(id: int, db: Session = Depends(main.get_db)):
    try:
        categoria = crud.get_categoria(db, categoria_id = id)
        return categoria
    except Exception as exc:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Categoria no encontrada")

# ======================================================================================================================

@app_v2.get("/orm/editoriales", response_model = List[schemas.Editorial_Get], status_code=status.HTTP_200_OK )
async def get_editoriales(offset: int = 0, limite: int = 100, db: Session = Depends(main.get_db)):
    try:
        editorial = crud.get_editoriales(db, offset, limite)
        return editorial
    except Exception as exc:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Editoriales no encontradas")


@app_v2.get("/orm/editorial/{id}", response_model = schemas.Editorial_Get, status_code= status.HTTP_200_OK)
def get_editorial(id: int, db: Session = Depends(main.get_db)):
    try:
        editorial = crud.get_editorial(db, editorial_id = id)
        return editorial
    except Exception as exc:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Editorial no encontrada")

# ==================================================================

@app_v2.get("/orm/libros", response_model = List[schemas.Libro_Get], status_code=status.HTTP_200_OK )
def get_libros(offset: int = 0, limite: int = 100, db: Session = Depends(main.get_db)):
    try:
        libros = crud.get_libros(db, offset, limite)
        return libros
    except Exception as exc:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Libros no encontrados")

@app_v2.get("/orm/libros/autor/{id_autor}", response_model = List[schemas.Libro_Get], status_code=status.HTTP_200_OK )
def get_libros_by(id_autor:int, db: Session = Depends(main.get_db)):
    try:
        libros = crud.get_libros_by_autor(db, id_autor)
        return libros
    except:
       raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Libros no encontrados")
