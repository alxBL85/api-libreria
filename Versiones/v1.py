from fastapi import FastAPI, status, HTTPException
from Schemas import Editorial

from Database.funciones import crear_conexion, query_all, query_one, count_rows, insert_one
import Routers.Autores as Autores
import Routers.Categorias as Categorias


tags_metadata = [
    {"name": "Autores", "description": "Servicios para administrar los autores de libros"},
    {"name": "Categorias", "description": "Servicios para administrar las categorías de libros"},
    {"name": "Editoriales", "description": "Servicios para administrar las editoriales"}
]


app_v1 = FastAPI(root_path="/v1", openapi_tags=tags_metadata)
app_v1.include_router(Autores.router)
app_v1.include_router(Categorias.router)



# ----------------------------------------------------------------------------------------------------------------------
@app_v1.post("/editorial", status_code = status.HTTP_201_CREATED, tags = ["Editoriales"])
async def crear_editorial(editorial: Editorial):
    '''
    Permite agregar una nueva editorial
    '''

    con = crear_conexion()
    dict_editorial = dict(editorial)
    dict_editorial["id"] = count_rows(con, table_name="editorial", indexed_column_name="id") + 1
    insert_query = "INSERT INTO EDITORIAL(id, nombre, pais) VALUES({id}, '{nombre}', '{pais}')"
    insert_one(con, insert_query, dict_editorial)
    con.close()
    return {'created': 'ok', 'id': dict_editorial["id"]}


