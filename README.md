# CREACION DE APIs CON PYTHON

---

## INSTRUCCIONES
### EJECUCIÓN
consola: uvicorn main:app --reload --port=8181

### DOCUMENTACIÓN
browser: http://127.0.0.1:8181/v1/docs

### COMANDOS COMUNES DE PYTHON

**Entornos Virtuales:**
Un folder que contiene una instalación de Python más los paquetes adicionales.

**Crear entorno virtual:**
python3 -m venv .venv

Una vez creado el entorno virtual, hay que activarlo:

.venv\Scripts\activate.bat 

> También existen versiones .ps1 y .sh, para powershell y linux

Activado el entorno virtual, podemos instalar paquetes dentro de él:

* python -m pip install nombre_paquete (instala la versión más reciente de un paquete)
* python -m pip install nombre_paquete === 1.1.1 (una version en específico del paquete)
* python -m pip install --upgrade nombre_paquete (actualiza un paquete)
* python -m pip uninstall nombre_paquete (desinstala un paquete)

Para conocer la información de un paquete:

    pip show nombre_paquete

Para listar todos los paquetes instalados:

    pip list
    pip freeze

Pip freeze retorna una lista de paquetes similar, pero incluye la versión del paquete instalado, este formato es el ideal recomendado por **pip install** para instalaciones nuevas de un proyecto. Normalmente esta lista se incluye en un archivo de texto llamado **requirements.txt**

    pip freeze > requirements.txt

Si se incluye el archivo **requirements.txt**, los usuarios nuevos pueden instalar todos los paquetes de un proyecto de la siguiente manera:

    python -m pip install -r requirements.txt



