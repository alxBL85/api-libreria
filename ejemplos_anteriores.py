from fastapi import FastAPI
from datetime import date
from typing import Optional
from Calculadora import operacionesBasicas

# ----------------------------------------------------------------------------------------------------------------------

@app.get("/saludar/{nombre}/{anio}")
async def getSayHi(nombre: str, anio: int):
    hoy = date.today()
    anioActual = hoy.year
    edad = anioActual - anio
    return { 'Hola '+nombre+', tienes '+str(edad)+' años !'}


@app.get('/empleados')
async def getEmpleados(apellido: str):
    return { "mensaje" : "Buscando empleados por apellido", "apellido" : apellido }


@app.get("/comparar/{a}/{b}")
async def comparar(a: int, b: int):
    resultado = "Mayor: "

    if a > b:
        resultado += str(a)
    elif a < b:
        resultado += str(b)
    elif a == b:
        resultado = "Ambos son iguales: "+str(a)

    return { resultado }

# ------------------------------------------------------------------------------------------------------------------

@app.get("/validarTarjeta/{genero}/{edad}/{trabaja}/{estado}")
async def validar(genero: str, edad: int, trabaja: str, estado: str):
    razon = ''

   # caso 1
    if (genero == 'mujer' and edad > 20 and trabaja == 'si' and estado == 'soltera'):
        return { "valido": 'si' }
    elif (genero != 'mujer'):
        razon = "genero no valido"
    elif (edad <= 20):
        razon += ",edad menor a 20"
    elif (trabaja != 'si'):
        razon += ",no trabaja"
    elif (estado != 'soltera'):
        razon += ",no es soltera"

   # caso 2
    if(genero == 'mujer' and edad > 30 and estado == 'casada'):
        return { "valido": 'si' }
    elif (genero != 'mujer'):
        razon = "genero no valido"
    elif (edad <= 30):
        razon += ", edad menor a 30"
    elif (estado != "casada"):
        razon += ", no es casada"

    return {
        "valido": 'no',
        "razones": razon
    }

# --------------------------------------------------------------------------------------------------------------------

@app.get("/dia/{numero}")
async def nombreDia(numero: int):
    nombres = [ "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO", "DOMINGO", 10, True, { 'valor': 1}]
    nombres.append("HOLA")
    nombres.remove(nombres[0])

    if numero >= 0 and numero < len(nombres):
        return {
            'dia': nombres[numero],
            'dias': nombres
        }
    else:
        return {
            'error': 'numero inválido'
        }


# -------------------------------------------------------------------------------------------------------------------


@app.get("/pares/{top}")
async def pares(top: int):
    indice = 0
    retorno = []
    while indice <= top:
        if indice % 2 == 0:
            retorno.append(indice)
        indice += 1

    return { 'pares': retorno }

# -------------------------------------------------------------------------------------------------------------------

@app.get("/ordenar")
async def getPalabras(palabra: str = None):
    ordenInverso = ''

    for letra in palabra:
        ordenInverso = letra + ordenInverso

    return ordenInverso


@app.get("/calculadora/{op}/{val1}/{val2}")
async def calc(op: str, val1: int, val2: int ):
    return operacionesBasicas.calculadora(valor = [val1, val2], operacion = op)

# ----------------------------------------------------------------------------------------------------------------------
########################################################################################################



lista_personas = []

# 2. Crear una clase que hereda de BaseModel y me dice como es el request body que recibiré

class Persona(BaseModel):
    id: int
    nombre: str
    apellido: str
    genero: str
    status: str = "Candidato"
    resultado: str = "No aprobado"

#3. Agregar un nueva persona:

@app.post("/rrhh/persona", status_code = status.HTTP_201_CREATED)
async def create_item(candidato: Persona): # recibimos un nuevo registro
    lista_personas.append(candidato)                           # almacenamos
    return { "id": len(lista_personas)-1}                 # id del registro

@app.get("/rrhh/personas")
async def list_items():
    return {"items": lista_personas }

@app.put("/rrhh/persona/{id}", status_code = status.HTTP_202_ACCEPTED)
async def update_item(id: int, nuevo_candidato: Persona):
    try:
        lista_personas[id] = nuevo_candidato
        return { "updated": "ok"}
    except IndexError:
        raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, detail = [{ "descripcion": "Candidato no encontrado o indice invalido"}])
    except:
        raise HTTPException(status_code = status.HTTP_400_BAD_REQUEST, detail = [{ "descripcion": "Excepcion no controlada"}])



