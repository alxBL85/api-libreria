def calculadora(operacion: str, valor):
    resultado = 0
    if(operacion == 'suma'):
        resultado = sum(valor)
    elif(operacion == 'resta'):
        resultado = valor[0]-valor[1]
    elif(operacion == 'mult'):
        resultado = valor[0]*valor[1]
    elif(operacion == 'div' and valor[1] != 0):
        resultado = valor[0]/valor[1]
    else:
        resultado = 'error'

    return resultado