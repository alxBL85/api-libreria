# CRUD: CREATE, READ, UPDATE, DELETE

from sqlalchemy.orm import Session
from . import models, schemas

def get_categoria(db: Session, categoria_id: int):
    return db.\
        query(models.Categoria).\
        filter(models.Categoria.id == categoria_id).\
        first()

def get_categorias(db: Session, skip: int = 0, limite: int = 100):
    return db.\
        query(models.Categoria).\
        offset(skip).\
        limit(limite).\
        all() # limit top

# ==================================================================================================

def get_editorial(db: Session, editorial_id: int):
    return db.\
        query(models.Editorial).\
        filter(models.Editorial.id == editorial_id).\
        first()

def get_editoriales(db: Session, offset: int = 0, limite: int = 100):
    return db.\
        query(models.Editorial).\
        offset(offset).\
        limit(limite).\
        all()

# ======================================================================================================

def get_libro(db: Session, libro_id: int):
    return db.\
        query(models.Libro).\
        filter(models.Libro.id == libro_id).\
        first()

def get_libros(db: Session, offset: int = 0, limite: int = 100):
    return db.\
        query(models.Libro).\
        offset(offset).\
        limit(limite).\
        all()

def get_libros_by_autor(db: Session, id_autor: int ):
    return db.query(models.Autor).filter(models.Autor.id == id_autor).first().libros


