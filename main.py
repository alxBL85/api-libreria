from fastapi import FastAPI
from Versiones.v1 import app_v1
from Versiones.v2 import app_v2

app = FastAPI()
app.mount("/v1", app_v1)
app.mount("/v2", app_v2)

@app.get("/version")
async def version():
    curso = 'Python'  # Cadena de Caracteres
    version = 1  # Valores Enteros
    subVersion = 0.5  # Valores de coma flotante
    habilitado = True  # Valores Booleanos (True / False)
    lastUpdated = '2021-08-01'

    version = str(version) + '.' + str(subVersion)

    return {'curso': curso,
            'version': version,
            'subversion': subVersion,
            'habilitado': habilitado,
            'actualizada': lastUpdated
            }


# ----------------------------------------------------------------------------------------------------------------------

@app.get("/isalive")
async def getIsAlive():
    return {'yes'}

# ---------------------------------------------------------------------------------------------------------------------

