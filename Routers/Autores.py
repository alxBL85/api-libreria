from fastapi import APIRouter, status, HTTPException
from Schemas import Autor
from Database.funciones import crear_conexion, query_all, query_one

router = APIRouter()

@router.get("/autores", tags=["Autores"])
async def get_autores():
    '''
    Servicio que devuelve un listado de todos los autores de libros
    :return: lista de autores
    '''

    con = crear_conexion()
    sql_query = "SELECT * FROM autor"
    resultados = query_all(con, sql_query)
    con.close()
    return {"autores": resultados}


# ----------------------------------------------------------------------------------------------------------------------

@router.post("/autor", tags=["Autores"], status_code=status.HTTP_201_CREATED)
async def crear_autor(autor: Autor):
    try:
        con = crear_conexion()
        dict_autor = dict(autor)
        dict_autor["id"] = query_one(con, "SELECT COUNT(id) + 1 FROM autor", id_column=0)
        sql_query = "INSERT INTO AUTOR(id, nombre, apellido, pais, nacimiento, fallecimiento) \
                                VALUES({id}, '{nombre}', '{apellido}', '{pais}', '{nacimiento}', '{fallecimiento}')"
        cur = con.cursor()
        cur.execute(sql_query.format(**dict_autor))
        con.commit()
        cur.close()
        con.close()
        return {'created': 'ok', 'id': dict_autor["id"]}
    except Exception as exc:
        con.rollback()
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail={"code": "exception:autor.no_creado"})

# ----------------------------------------------------------------------------------------------------------------------

@router.delete("/autor/{id}", status_code = status.HTTP_200_OK, tags=["Autores"])
async def delete_autor(id: int):
     try:
        con = crear_conexion()
        sql_query = f"DELETE FROM autor WHERE id = {id}"
        cur = con.cursor()
        cur.execute(sql_query)
        con.commit()
        cur.close()
        con.close()
        return {'deleted': 'ok'}
     except:
        con.rollback()
        raise HTTPException(status_code= status.HTTP_400_BAD_REQUEST, detail = {"code":"expection:autor.no_borrado"})
