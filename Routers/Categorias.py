from fastapi import APIRouter, status, HTTPException

from Database.funciones import crear_conexion, query_all, query_one

router = APIRouter()

# /v1/categorias
@router.get("/categorias", tags=["Categorias"])
async def get_categorias():
    con = crear_conexion()
    sql_query = "SELECT id, nombre FROM categoria"
    resultados = query_all(con, sql_query)
    con.close()
    return {"categorias": resultados}


# /v1/categoria/{idCat}
@router.get("/categoria/{idCat}", tags=["Categorias"])
async def get_categoria(idCat: int):
    con = crear_conexion()
    sql_query = f"SELECT * FROM categoria c WHERE c.id = {idCat}"
    resultado = query_one(con, sql_query, id_column = 1)
    print(resultado)
    con.close()

    if (resultado is None):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="exception:categoria.no_encontrado")
    else:
        return {"categoria": resultado}
