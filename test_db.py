from fastapi import FastAPI
from Database.funciones import crear_conexion, query_all

app = FastAPI()

con = crear_conexion()
sql_query = "SELECT * FROM categoria"
resultados = query_all(con, sql_query)
print(resultados)
con.close()
